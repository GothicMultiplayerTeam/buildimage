ARG IMAGE_NAME=ubuntu
ARG IMAGE_TAG=20.04
ARG ARCHITECTURE=amd64

FROM ${ARCHITECTURE}/${IMAGE_NAME}:${IMAGE_TAG}
ARG ARCHITECTURE

# fix for error under arm-64 related to libc-bin
RUN rm /var/lib/dpkg/info/libc-bin.*

# update repositories
RUN apt-get update

# install build essential packages
RUN apt-get install -y -q \
    libc-bin \
    build-essential \
    ninja-build \
    libcurl4-openssl-dev \
    zlib1g-dev

 # install cmake compilation required packages
RUN apt-get install -y -q \
    libssl-dev \
    zip \
    unzip

# install vcpkg required packages
RUN export DEBIAN_FRONTEND=noninteractive && apt-get install -y -q \
    pkg-config

# install utility packages
RUN apt-get install -y -q \
    curl \
    git

# upgrade packages
RUN apt-get upgrade -y
RUN apt-get clean

# install cross-compilers for armhf and arm64 for x64 image
RUN if [ "$ARCHITECTURE" = "amd64" ]; then \ 
        apt-get install -y -q gcc-arm-linux-gnueabihf g++-arm-linux-gnueabihf binutils-arm-linux-gnueabihf; \
        apt-get install -y -q gcc-aarch64-linux-gnu g++-aarch64-linux-gnu binutils-aarch64-linux-gnu; \
    fi

# install cmake from source
COPY install-cmake.sh .
RUN chmod +x install-cmake.sh && ./install-cmake.sh

# install vcpkg
COPY install-vcpkg.sh .
RUN chmod +x install-vcpkg.sh && ./install-vcpkg.sh