# Official Gothic 2 Online build image
Build image created for compilation of Gothic 2 Online. Includes newest CMake and GCC for i686, armhf and arm64.

## Compilation for x64
To compile binary for 64-bit, use this compilers:
```bash
i686-linux-gnu-gcc
i686-linux-gnu-g++
```

## Cross compilation for ARM
To cross-compile binary for 32-bit arm with hardware floats support, use this compilers:
```bash
arm-linux-gnueabihf-gcc
arm-linux-gnueabihf-g++
```

To cross-compile binary for 64-bit arm, use this compilers:
```bash
aarch64-linux-gnu-gcc
aarch64-linux-gnu-g++
```
