#!/bin/sh

# Download the vcpkg
git clone https://github.com/microsoft/vcpkg.git

# Run bootstrap.sh
chmod +x /vcpkg/bootstrap-vcpkg.sh
./vcpkg/bootstrap-vcpkg.sh

# Create vcpkg symlink, so that vcpkg can be used globally
ln -s /vcpkg/vcpkg /usr/local/bin/vcpkg

# Remove yourself
rm -- "$0"